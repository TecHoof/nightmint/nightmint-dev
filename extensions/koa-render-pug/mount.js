/**
 * Render pug templates.
 */

const
    Render = require('koa-pug'),
    debud = require('debug')('extensions:koa-render-pug');

const
    { app, static } = require('@'),
    packageConfig = require('@:config/koa-render-pug');


// Export module
module.exports = new Render({
    app,
    
    viewPath: static.getPath('templates'),
    debug: packageConfig.debug,
    pretty: packageConfig.pretty,
    compileDebug: packageConfig.compileDebug,
});
