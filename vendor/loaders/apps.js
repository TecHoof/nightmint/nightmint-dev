const
    fs = require('fs'),
    debug = require('debug')('vendor:loaders:apps');

const { aliases } = require('@');

const APPS_FOLDER = `${__dirname}/../../apps`;


var
    apps = [],
    dirs = fs.readdirSync(APPS_FOLDER);


dirs.forEach(app => {
    apps.push(app);
    aliases.addAlias(`$:${app}`, `${APPS_FOLDER}/${app}`);
});

dirs.forEach(app => {
    let routesPath = `${APPS_FOLDER}/${app}`;

    if(fs.existsSync(routesPath))
        require(routesPath);
});


module.exports = apps;
