/**
 * RUN COPY IF NOT EXIST IN PROJECT GENERAL (META) CONFIG FILES
 */

const
  fs = require('fs'),
  path = require('path'),
  debug = require('debug')('vendor:loaders:meta');

const {
  bootstrap
} = require('@');

const META_FOLDER = `${__dirname}/../../config/meta`;


fs
  .readdirSync(META_FOLDER)
  .forEach(configFile => {
    bootstrap.genConfFile(path.join(META_FOLDER, configFile), configFile);
  });
