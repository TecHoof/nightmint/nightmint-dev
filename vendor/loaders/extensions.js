const
    fs = require('fs'),
    debug = require('debug')('vendor:loaders:extensions');

const { aliases } = require('@');

const PACKAGES_FOLDER = `${__dirname}/../../extensions`;
const CONFIG_FOLDER = `${__dirname}/../../config`;

let packages = [];

fs
    .readdirSync(PACKAGES_FOLDER)
    .forEach(package => {
        packages.push(package);
        
        let packagePath = `${PACKAGES_FOLDER}/${package}/`;
        
        let mountPath = `${PACKAGES_FOLDER}/${package}/mount.js`;

        aliases.addAlias(`&:${package}`, mountPath);
        debug(mountPath);

        if (fs.existsSync(`${packagePath}/config.example.js`) && !fs.existsSync(`${CONFIG_FOLDER}/${package}.js`)) {
            fs.copyFileSync(`${packagePath}/config.example.js`, `${CONFIG_FOLDER}/${package}.js`);
        }
        
        if (fs.existsSync(mountPath)) {
            require(mountPath);
        }
    });


module.exports = packages;
