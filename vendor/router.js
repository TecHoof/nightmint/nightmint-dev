/**
 * Router app. Include any app controllers.
 */

const
    Router = require('koa-router'),
    debug = require('debug')('vendor:router');


// Router
const router = new Router();


module.exports = router;
