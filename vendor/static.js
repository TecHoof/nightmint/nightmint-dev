/*
 * Handler project statics path
 */

const
    Path = require('path'),
    Statics = require('koa-static'),
    fs = require('fs'),
    os = require('os'),
    debug = require('debug')('vendor:static ]');

const staticConfig = require('@:config/static');


/** Special class for build static path */
class PathBuilder {
    constructor(path, pathways) {
        this.list = {};
        this.root = Path.join(__dirname, path);
        this.initList(pathways);
    }

    /**
     * Return full path to arguent folder
     * @param {String} path 
     */
    build(name, nameDir) {
        const e = Path.join(this.root, nameDir);
        this.list[name] = e;

        return e;
    }

    /**
     * Initial PathBuilder list from config pathways
     * @param {Object} pathways 
     */
    initList(pathways) {
        for (const name in pathways) {
            this.build(name, pathways[name]);
        }
    }

    /**
     * Build path to file in {name} static folder
     * @param {String} name 
     * @param {String} nameFile 
     */
    toFile(name, nameFile) {
        return Path
            .join(this.list[name], nameFile.toString())
            .toString();
    }

    /**
     * Save new file
     * @param {File} file 
     * @param {String} newNameFile 
     * @param {String} nameDir 
     */
    saveFile(file, newNameFile, nameDir) {
        try {
            const reader = fs.createReadStream(file.path);
            const stream = fs.createWriteStream(
                this.toFile(nameDir, newNameFile));

            reader.pipe(stream);

            debug(`Save file ${newNameFile} -> ${nameDir}`);
        } catch (error) {
            debug(`Faile save file ${newNameFile} -> ${nameDir}`, error);
        }
    }

    /**
     * Movefile to new location
     * @param {File} file 
     * @param {String} newNameFile 
     * @param {String} nameDir 
     */
    moveFile(file, newNameFile, nameDir) {
        try {
            fs.rename(file.path, this.toFile(nameDir, newNameFile));
        } catch (error) {
            debug(`Faile move file ${newNameFile} -> ${nameDir}`, error);
        }
    }

    /**
     * Read sync file
     * @param {String} name 
     * @param {String} nameFile 
     */
    readFile(name, fileName) {
        return fs.readFileSync(this.toFile(name, fileName)).toString();
    }

    /** Return list static pathways */
    getList() {
        return this.list;
    }

    /**
     * Func return full path to folder from static pathways
     * @param {String} name 
     */
    getPath(name) {
        return this.list[name];
    }

    /**
     * Middleware for app
     * Return instance koa-static by path by folder name
     * @param {String} name 
     */
    Statics(name) {
        return Statics(this.getPath(name));
    }
}

// Create builder instance
module.exports = new PathBuilder('../', staticConfig.pathways);
