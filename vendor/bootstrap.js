/**
 * BOOSTSRAP
 *
 * Run after create app for configurate server
 */

const
    fs = require('fs'),
    path = require('path'),
    debug = require('debug')('vendor:bootstrap');

const BootstrapError = require("@:vendor/errors").BootstrapError;

const CONFIG_FOLDER = `${__dirname}/../config`;


function compareConfig(confExample, conf) {
    const
        confExampleF = require(confExample),
        confF = require(conf);

    for (key in confExampleF) {
        if (!confF.hasOwnProperty(key)) {
            throw new BootstrapError(`Key "${key}" not found in ${conf}`);
        }
    }

    return true;
}

function genConfFile(pathExample, name) {
    const filePath = path.join(CONFIG_FOLDER, name);

    if (fs.existsSync(filePath) && compareConfig(pathExample, filePath)) {
        debug(`Config "${name}" already created. OK.`);

    } else if (fs.existsSync(pathExample)) {
        fs.copyFileSync(pathExample, path.join(CONFIG_FOLDER, name));

    } else {
        throw `Config file not found from path [${pathExample}]`;
    }
}


module.exports = {
    genConfFile
};
