const
    path = require('path')
    debug = require('debug')('app:aliases');


const defaultModule = require('module');
const package = require(`${__dirname}/package`);

if(!package.hasOwnProperty('aliases')) {
    throw new Error(`Property "aliases" not found in your package.json`);
}

var aliases = new Map(Object.entries(package.aliases));


defaultModule._resolveFilename = new Proxy(defaultModule._resolveFilename, {
    apply: (target, thisArg, argumentsList) => {
        aliases.forEach((modulePath, alias) => {
            if (argumentsList[0] === alias) {
                argumentsList[0] = `${path.resolve(path.dirname(require.main.filename), modulePath)}`;
            }

            if (argumentsList[0].includes(`${alias}/`)) {
                argumentsList[0] = `${path.resolve(path.dirname(require.main.filename), modulePath)}${argumentsList[0].split('/').length > 1 ? argumentsList[0].substr(argumentsList[0].indexOf("/"), argumentsList[0].length - 1) : ''}`;
            }
        });

        return target.apply(thisArg, argumentsList);
    }
});


function addAlias(key, path) {
    aliases.set(key, path);
}

function getPath(key) {
    return aliases.get(key);
}

module.exports = {
    getPath,
    addAlias,

    PACKAGE_JSON: package
};
