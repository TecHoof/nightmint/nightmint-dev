const debug = require('debug')('apps:common:controllers:home');

module.exports = {
    /**
     * Home view handler
     */
    index: async (ctx, next) => {
        await ctx.render("home");
    }
}
