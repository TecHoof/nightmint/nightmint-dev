/**
 * COMMON ROUTES
 */

const { router } = require('@');

const
    HomeCtr = require('$:common/controllers/home');


router
    .get('/', HomeCtr.index);