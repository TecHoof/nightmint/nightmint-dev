/**
 * General project file
 */

const aliases = require('./aliases');

const
    Koa = require('koa'),
    session = require('koa-session'),
    bodyParser = require('koa-bodyparser'),
    debug = require('debug')('app:main');


/**
 * Generate and check project configs
 */
module.exports.aliases = aliases;

const bootstrap = require('@:vendor/bootstrap');
module.exports.bootstrap = bootstrap;

require('@:vendor/loaders/meta');


/**
 * Project files
 */

const config = require('@:config/app');


/**
 * Create application and load config
 */

var app = new Koa();

app.config = config;
app.keys = config.sessionKeys;


/** Start application */
const server = app.listen(config.port, () => {
    debug('Koa is listening:', server.address());
});


/** Export module */
module.exports.app = app;
module.exports.server = server;
module.exports.config = config;


/**
 * Configure use general packages
 */


app
    .use(bodyParser())
    .use(session(config.koaSession, app));


/**
 * Bootstrap module load and use
 */

const
    router = require('@:vendor/router'),
    static = require('@:vendor/static');

app
    .use(static.Statics('assets'))
    .use(static.Statics('node_modules'));


module.exports.router = router;
module.exports.static = static;


/**
 * Loaders
 */
const
    extensions = require('@:vendor/loaders/extensions'),
    apps = require('@:vendor/loaders/apps');


app
    .use(router.routes())
    .use(router.allowedMethods());
