module.exports = {
    debug: false,
    session_keys: ['magic', 'pony'],
    domain: 'http://localhost:3000',
    port: process.env.PORT || 3000,
    proxy: true
};
